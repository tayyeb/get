#!/bin/bash
# USAGE:  curl -sSL https://gitlab.com/tayyeb/get/raw/file/lxd/install-python.sh | bash
# USAGE:  get lxd/install-python.sh
set -e

echo -e "\e[96m### Installing python 2.7, pip, virtualenv ...\e[0m"
apt update
apt install -y python-pip \
               libmysqlclient-dev nodejs
               
pip install --upgrade pip virtualenv

echo -e "\e[96m### Installing python 2.7, pip, virtualenv ...  DONE.\e[0m"
