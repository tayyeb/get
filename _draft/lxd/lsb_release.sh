#!/bin/bash

remote="$1"
list=`lxc list $remote -c n | grep -v -- -- | sed 's/|//g' | sed 's/^ //g' | tail -n +2`
readarray -t containers <<<"$list"

for cname in "${containers[@]}"
do
    lsb=`lxc exec ${remote}${cname} -- lsb_release -ds 2> /dev/null`
    echo "${remote}${cname}  -  $lsb"
done
