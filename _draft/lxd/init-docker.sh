#!/bin/bash
# USAGE:  curl -sSL https://gitlab.com/tayyeb/get/raw/file/lxd/init-docker.sh | bash
set -e

echo -e "\e[96m### Installing docker form ubuntu (docker.io)\e[0m"
apt install docker.io -y

echo -e "\e[96m### Installing docker-compose form docker repo (version 1.11.2)\e[0m"
curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo -e "\e[96m### docker and docker-compose are ready\e[0m"
