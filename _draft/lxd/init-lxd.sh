#!/bin/bash
# USAGE: curl -sSL https://gitlab.com/tayyeb/get/raw/file/host/init-lxd.sh | bash
set -e

apt install software-properties-common
add-apt-repository -y ppa:ubuntu-lxc/lxd-stable

apt update
apt dist-upgrade -y
apt install -y lxd zfsutils-linux
