#!/bin/bash
# USAGE: curl -sSL https://gitlab.com/tayyeb/get/raw/file/lxc-launch.sh | sudo bash
# and after first installation
# USAGE:
#   lxc-launch -> update lxc-launch script
#   lxc-launch ubuntu:16.04 c1 -> lxc launch ubuntu:16.04 c1 , intall get on it
set -e

URL=https://gitlab.com/tayyeb/get/raw/file

if [ "$1" == "" ]; then
  echo -e "\e[96m### installing/updating lxc-launch\e[0m"
  curl -sSL ${URL}/lxc-launch.sh > /usr/local/bin/lxc-launch
  chmod +x /usr/local/bin/lxc-launch
  exit 0;
fi

WAIT=10
lxc launch $@ || :
echo "sleep ${WAIT}s to let container network ready ..." && sleep ${WAIT}
lxc exec $2 -- sh -c "curl -sSL ${URL}/lxd/init.sh | bash"
