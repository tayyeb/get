#!/bin/bash
# USAGE: curl -sSL https://gitlab.com/tayyeb/get/raw/file/lxd/init.sh | bash
set -e

curl -sSL https://gitlab.com/tayyeb/get/raw/file/init.sh | bash

echo -e "\e[96m### stopping unused services... \e[0m"
systemctl stop snapd.service snapd.socket snapd.refresh.timer
systemctl disable snapd.service snapd.socket snapd.refresh.timer snapd.autoimport.service snapd.system-shutdown.service

systemctl stop cron.service && systemctl disable cron.service
systemctl stop atd.service && systemctl disable atd.service
systemctl stop ssh.service && systemctl disable ssh.service
systemctl stop apt-daily.timer && systemctl disable apt-daily.timer

# systemctl stop getty.target
# systemctl disable getty@.service
# systemctl mask getty.target

echo -e "\e[96m### lxd/init.sh done\e[0m"
