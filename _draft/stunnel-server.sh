#!/bin/bash
#
# Usage curl -sSL https://gitlab.com/tayyeb/get/raw/file/apps/stunnel-server.sh | BACKEND_NAME=redis-server LOCAL_PORT=26379 REMOTE_PORT=6379 sh
#

set -e

# default value to its initials
: ${LOCAL_PORT:=26379}
: ${REMOTE_PORT:=6379}
: ${BACKEND_NAME:=stunnel-server}
: ${CERTFILE:=/etc/stunnel/${BACKEND_NAME}.pem}

: ${ACCEPT:=0.0.0.0:${REMOTE_PORT}}
: ${CONNECT:=127.0.0.1:${LOCAL_PORT}}
: ${CAFILE:=/usr/local/share/ca-certificates/ca_tsaze.crt}


if [ ! -f ${CAFILE} ]; then
  curl -sSL https://gitlab.com/tayyeb/get/raw/file/init.sh | bash
fi

# install required packages
apt install -y stunnel4
service stunnel4 stop

# config stunnel
sed -i  -e "s/ENABLED=0/ENABLED=1/g" /etc/default/stunnel4

if [ ! -f "${CERTFILE}" ]; then
    echo -e "\e[96m### creating a self signed certificate for ${BACKEND_NAME} at ${CERTFILE}. feel free to change it with your own.\e[0m"
    openssl req -nodes -new -x509 -days 365 \
                -subj "/C=AM/ST=stunnel/O=${BACKEND_NAME}/commonName=${BACKEND_NAME}/organizationalUnitName=stunnel/emailAddress=stunnel@example.com" \
                -keyout ${CERTFILE} >> ${CERTFILE}
fi
chmod 600 ${CERTFILE}

cat <<eofCONF > /etc/stunnel/${BACKEND_NAME}.conf
# auto generated file with stunnel-server.sh
pid = /var/run/stunnel4/${BACKEND_NAME}.pid
setuid = stunnel4
setgid = stunnel4
socket = l:TCP_NODELAY=1
socket = r:TCP_NODELAY=1
socket = a:SO_REUSEADDR=1


[${BACKEND_NAME}]
cert = ${CERTFILE}
CAfile = ${CAFILE}
verify = 2
accept = ${ACCEPT}
connect = ${CONNECT}

# TIMEOUTidle = 10
TIMEOUTbusy = 10
TIMEOUTclose = 10
TIMEOUTconnect = 10
eofCONF

#configure the firewall
ufw allow ${REMOTE_PORT}

# start the services
service stunnel4 start

echo -e "\e[96m### stunnel server for ${BACKEND_NAME} configured on ${ACCEPT}\e[0m"
