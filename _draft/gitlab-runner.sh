#!/usr/bin/env bash
set -e

echo "### Installing gitlab-runner"
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.deb.sh | sudo bash
apt-get install -y gitlab-ci-multi-runner

usermod -aG sudo gitlab-runner
passwd --delete gitlab-runner

echo ">> please run a 'gitlab-runner register'"
