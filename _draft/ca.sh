#!/bin/bash
# USAGE:  curl -sSL https://gitlab.com/tayyeb/get/raw/file/common/ca.sh | bash
set -e

sudo apt-get install -y ca-certificates
sudo mkdir -p /usr/local/share/ca-certificates/
sudo curl -sSL https://gitlab.com/tayyeb/get/raw/file/common/ca_tsaze.crt > /usr/local/share/ca-certificates/ca_tsaze.crt
sudo update-ca-certificates
# interactive mode
# sudo dpkg-reconfigure ca-certificates
