#!/bin/bash
#
# Usage is one of below or mixture of them:
#   - curl -sSL https://gitlab.com/tayyeb/get/raw/file/apps/stunnel-client.sh | BACKEND_NAME=my-client REMOTE_HOST=example.com REMOTE_PORT=1234 LOCAL_PORT=1234 sh
#   - curl -sSL https://gitlab.com/tayyeb/get/raw/file/apps/stunnel-client.sh | BACKEND_NAME=my-client REMOTE_HOST=example.com PORT=1234 sh
#   - curl -sSL https://gitlab.com/tayyeb/get/raw/file/apps/stunnel-client.sh | BACKEND_NAME=my-client CONNECT=example.com:1234 LOCAL_PORT=1234 sh
#   - curl -sSL https://gitlab.com/tayyeb/get/raw/file/apps/stunnel-client.sh | BACKEND_NAME=my-client CONNECT=example.com:1234 ACCEPT=127.0.0.1:1234 sh
#

set -e

# default value to its initials
: ${PORT:=1234}
: ${LOCAL_PORT:=${PORT}}
: ${REMOTE_PORT:=${PORT}}
: ${REMOTE_HOST:=example.com}

: ${ACCEPT:=127.0.0.1:${LOCAL_PORT}}
: ${CONNECT:=${REMOTE_HOST}:${REMOTE_PORT}}

: ${BACKEND_NAME:=stunnel-client}
: ${CERTFILE:=/etc/stunnel/${BACKEND_NAME}.pem}

# install required packages
apt install -y stunnel4
service stunnel4 stop

# config stunnel
sed -i  -e "s/ENABLED=0/ENABLED=1/g" /etc/default/stunnel4

if [ ! -f "${CERTFILE}" ]; then
    echo -e "\e[96m### creating a self signed certificate for ${BACKEND_NAME} at ${CERTFILE}. feel free to change it with your own.\e[0m"
    openssl req -nodes -new -x509 -days 365 \
                -subj "/C=AM/ST=stunnel/O=${BACKEND_NAME}/commonName=${BACKEND_NAME}/organizationalUnitName=stunnel/emailAddress=stunnel@example.com" \
                -keyout ${CERTFILE} >> ${CERTFILE}
fi
chmod 600 ${CERTFILE}

cat <<eofCONF > /etc/stunnel/${BACKEND_NAME}.conf
# auto generated file with stunnel-client.sh
pid = /var/run/stunnel4/${BACKEND_NAME}.pid
setuid = stunnel4
setgid = stunnel4
socket = l:TCP_NODELAY=1
socket = r:TCP_NODELAY=1
socket = a:SO_REUSEADDR=1


[${BACKEND_NAME}]
client= yes
retry = yes
cert = ${CERTFILE}
accept = ${ACCEPT}
connect = ${CONNECT}

# TIMEOUTidle = 10
TIMEOUTbusy = 10
TIMEOUTclose = 10
TIMEOUTconnect = 10
eofCONF

#configure the firewall
ufw allow ${REMOTE_PORT}

# start the services
service stunnel4 start

echo -e "\e[96m### stunnel client for ${BACKEND_NAME} configured on ${ACCEPT} for ${CONNECT}\e[0m"
