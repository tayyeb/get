# get/the/raw/file/
I make this project with specific name to get some common configs fast and secure. they are not confidentials.

### my start point:

`curl -sSL https://gitlab.com/tayyeb/get/raw/file/init | bash`

which do bashrc, htop, get

### New node start point:

`wget -qO- https://gitlab.com/tayyeb/get/raw/file/new-node | bash`

which do init docker kubectl dist-upgrade and new-server

### $ get
`curl -sSL https://gitlab.com/tayyeb/get/raw/file/get | bash`

and then use it:

> `get init`  
> `get apps/kubectl`

####  for other files see the [REPOSITORY](https://gitlab.com/tayyeb/get/tree/file) 
